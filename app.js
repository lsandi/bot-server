var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var expresssession = require('express-session');
const mysql = require('mysql2/promise');
const sha256 = require('sha256');
require('dotenv').config();
const axios = require('axios');
const short = require('short-uuid');
const QRCode = require('qrcode');
const jsQR = require("jsqr");
const nodeFetch = require('node-fetch')
const jpeg = require('jpeg-js');
var firebase = require("firebase/app");
require('firebase/database');

var firebaseConfig = {
  apiKey: "AIzaSyBLm8_SXwHbMEjJ7yGcqRQ0VpVt_PEOeao",
  authDomain: "sls-trucklocation.firebaseapp.com",
  databaseURL: "https://sls-trucklocation.firebaseio.com",
  projectId: "sls-trucklocation",
  storageBucket: "sls-trucklocation.appspot.com",
  messagingSenderId: "401254905819",
  appId: "1:401254905819:web:eab25d5bc9720c673e59fe"
};

firebase.initializeApp(firebaseConfig);
const peers = process.env.PEERS.split(' ');
let peersindex = 0;
let count = 0;
function roundRobinPeers() {
  if (peersindex == (peers.length-1)){
    peersindex = 0
  } else {
    peersindex++;
  }
}


var app = express();

const db = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  multipleStatements: true
})
// view engine setup
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(expresssession({
  secret: process.env.SECRET,
  resave: true,
  saveUninitialized: true
}))
app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Terdapat error')
})

app.get('/', function(req, res, next) {
  res.sendFile(__dirname+'/views/login.html')
});

//https://codeshack.io/basic-login-system-nodejs-express-mysql/
app.post('/auth', function(req, res) {
  const username = req.body.username;
  const password = sha256(req.body.password);
  const type = req.body.type;

  if(type == 'pt') {
    db.query({
      sql: 'SELECT * FROM `users` WHERE username = ? AND password = ?',
      timeout: 40000, // 40s
      values: [username, password]
    }, function(error, results, fields) {
      if (error){
        console.log("ERROR", error.sqlMessage)
      }
      if (results.length > 0) {
        req.session.loggedin = true;
        req.session.type= 'pt';
        res.redirect('/homept');
      } else {
        res.send('Username / Password salah')
      }
      res.end();
    });
  } else if (type == 'st') {
    db.query({
      sql: 'SELECT * FROM `drivers` WHERE username = ? AND password = ?',
      timeout: 40000, // 40s
      values: [username, password]
    }, function(error, results, fields) {
      if (results.length > 0) {
        req.session.loggedin = true;
        req.session.type= 'st';
        res.redirect('/homest');
      } else {
        res.send('Username / Password salah')
      }
      res.end();
    })
  } else {
    res.send('Masukkan username & password');
    res.end();
  }
})

app.get('/homept', function(req, res) {
  if (req.session.loggedin) {
    res.sendFile(__dirname+'/views/homept.html')
  } else {
    res.redirect('/')
  }
})

app.get('/homest', function(req, res) {
  if (req.session.loggedin) {
    res.sendFile(__dirname+'/views/homest.html')
  } else {
    res.redirect('/')
  }
})

app.get('/homestwaybill', function(req, res) {
  if (req.session.loggedin) {
    res.sendFile(__dirname+'/views/homestwaybill.html')
  } else {
    res.redirect('/')
  }
})

app.post('/getptcode', function(req, res) {
  res.send(`https://t.me/sls2020_bot?start=${process.env.PT_TOKEN}`);
})

app.post('/routeqr', function(req, res) {
  let id = req.body.route_id;
  QRCode.toFile('./public/images/routeqr.png', sha256(id), ()=> {
    res.redirect('/downloadrouteqr');
  })
})

app.get('/downloadrouteqr', function(req, res) {
  res.download('./public/images/routeqr.png')
})

app.post('/waybillqr', function(req, res) {
  let id = req.body.waybill_id;
  QRCode.toFile('./public/images/waybillqr.png', sha256(id), ()=> {
    res.redirect('/downloadwaybillqr');
  })
})

app.get('/downloadwaybillqr', function(req, res) {
  res.download('./public/images/waybillqr.png')
})
// Telegram Bot

const Telegraf = require('telegraf')
const session = require('telegraf/session')
const Stage = require('telegraf/stage')
const Scene = require('telegraf/scenes/base')
const { leave } = Stage

const Markup = require('telegraf/markup')
const WizardScene = require('telegraf/scenes/wizard')

let regObject = {};
let truckObject = {};
let truckModelObject = {};
let orderObject = {};
let routeObject = {};
let waybillObject = {};
let paymentObject = {};
let routepaths = [];

let pt_id = process.env.CHAT_ID;

function getCurrentTimestamp() {
  let date = (new Date()).toJSON().replace("T", " ").replace("Z", "").split(' ');
  return date[0]
}

const bot = new Telegraf(process.env.BOT_TOKEN)
bot.start((ctx) => {
  if (ctx.startPayload === process.env.PT_TOKEN){
    pt_id = ctx.chat.id;
    ctx.reply(`Selamat Datang di Sistem Logistik Cerdas, Anda adalah Perusahaan Transportasi`)
  } else {
    ctx.reply(`Selamat Datang di Sistem Logistik Cerdas`)
    ctx.telegram.sendMessage(pt_id, `${ctx.chat.first_name} menekan start`)
    ctx.reply('Silahkan melakukan registrasi', 
    Markup.inlineKeyboard([
      Markup.callbackButton('Registrasi', 'register')
    ]).resize().extra())
  }
})

const userRegistrationScene = new WizardScene('user-registration',
  (ctx) => {
    ctx.reply('Nama:')
    ctx.wizard.next();
  },
  (ctx) => {
    regObject.id = short.generate();
    regObject.name = ctx.message.text;
    ctx.reply('Alamat:')
    return ctx.wizard.next()
  },
  (ctx) => {
    regObject.address = ctx.message.text;
    ctx.reply('Nomor telepon:')
    return ctx.wizard.next()
  },
  (ctx) => {
    regObject.phone = ctx.message.text;
    regObject.chat_id = ctx.chat.id;
    ctx.telegram.sendMessage(pt_id,
      `
      Registrasi Pengguna Baru
      ========================

      ID: ${regObject.id}

      Nama: ${regObject.name}

      Alamat: ${regObject.address}

      No. Telepon: ${regObject.phone}

      Silahkan lakukan konfirmasi untuk menambahkan pengguna dengan menggunakan /reguser SPASI {ID pengguna}
      `
      )
    ctx.reply('Input Data Registrasi Selesai, Menunggu Konfirmasi')
    return ctx.scene.leave()
  }
)

const driverRegistrationScene = new WizardScene('driver-registration',
  (ctx) => {
    ctx.reply('Nama:')
    ctx.wizard.next();
  },
  (ctx) => {
    regObject.id = short.generate();
    regObject.name = ctx.message.text;
    ctx.reply('NIK:')
    return ctx.wizard.next()
  },
  (ctx) => {
    regObject.nik = ctx.message.text;
    ctx.reply('Usia:')
    return ctx.wizard.next()
  },
  (ctx) => {
    regObject.age = ctx.message.text;
    ctx.reply('Nomor telepon:')
    return ctx.wizard.next()
  },
  (ctx) => {
    regObject.phone = ctx.message.text;
    regObject.chat_id = ctx.chat.id;
    ctx.telegram.sendMessage(pt_id,
      `
      Registrasi Sopir Baru
      ========================

      ID: ${regObject.id}

      Nama: ${regObject.name}

      NIK: ${regObject.nik}

      Usia: ${regObject.age}

      No. Telepon: ${regObject.phone}

      Silahkan lakukan konfirmasi untuk menambahkan sopir dengan menggunakan /regdriver SPASI {ID sopir} SPASI {username sopir}#{password sopir}
      `
      )
    ctx.reply('Input Data Registrasi Selesai, Menunggu Konfirmasi')
    return ctx.scene.leave()
  }
)

const addTruckModelScene = new WizardScene('add-truckmodel',
  (ctx) => {
    truckModelObject.id = short.generate();
    ctx.reply('Nama Model:')
    return ctx.wizard.next()
  },
  (ctx) => {
    truckModelObject.name = ctx.message.text;
    ctx.reply('Kapasitas (m3):')
    return ctx.wizard.next()
  },
  (ctx) => {
    truckModelObject.capacity = ctx.message.text;
    ctx.reply(
      `
      Tambah Model Truk Baru
      ========================

      ID: ${truckModelObject.id}

      Nama: ${truckModelObject.name}

      Kapasitas: ${truckModelObject.capacity}

      Apakah Anda mau menambahkan model truk ini?
      `
      ,Markup.inlineKeyboard([
        Markup.callbackButton('✅ Ya', 'newmodeltruck-yes'),
        Markup.callbackButton('❌ Tidak', 'newmodeltruck-no'),
      ]).extra())

    return ctx.scene.leave()
  }
)

//mockup model
let models = []

const addTruckScene = new WizardScene('add-truck',
  (ctx) => {
    let modeltext = 'No.    Nama      Kapasitas\n'
    models.forEach((model, index) => {
      modeltext = modeltext + `${index+1}.      ${model.name}       ${model.capacity}\n`
    });
    ctx.reply('Model:')
    ctx.reply(modeltext);
    ctx.reply('Pilih dengan memasukkan angka, contoh: 1')
    ctx.wizard.next();
  },
  (ctx) => {
    truckObject.id = short.generate();
    truckObject.model_id = models[ctx.message.text-1].id;
    truckObject.model_name = models[ctx.message.text-1].name;
    ctx.reply('Kode Truk:')
    return ctx.wizard.next()
  },
  (ctx) => {
    truckObject.code = ctx.message.text;
    ctx.reply('Vendor:')
    return ctx.wizard.next()
  },
  (ctx) => {
    truckObject.vendor = ctx.message.text;
    ctx.reply('Seri:')
    return ctx.wizard.next()
  },
  (ctx) => {
    truckObject.series = ctx.message.text;
    ctx.reply(
      `
      Tambah Truk Baru
      ========================

      ID: ${truckObject.id}

      Model: ${truckObject.model_name}

      Kode: ${truckObject.code}

      Vendor: ${truckObject.vendor}

      Seri: ${truckObject.series}

      Apakah Anda mau menambahkan truk ini?
      `
      ,Markup.inlineKeyboard([
        Markup.callbackButton('✅ Ya', 'newtruck-yes'),
        Markup.callbackButton('❌ Tidak', 'newtruck-no'),
      ]).extra())

    return ctx.scene.leave()
  }
)

//mockup penerima
let receivers = []

let numitem = 0;
let itemincrement = 1;
let itemstartstep = 0;

const addOrderScene = new WizardScene('add-order',
  (ctx) => {
    let receivertext = 'No.    Nama      Alamat\n'
    receivers.forEach((receiver, index) => {
      receivertext = receivertext + `${index+1}.      ${receiver.name}       ${receiver.address}\n`
    });
    ctx.reply('Penerima:')
    ctx.reply(receivertext);
    ctx.reply(`Pilih dengan memasukkan angka, contoh: 1
    *Penerima harus melakukan registrasi terlebih dahulu`)
    ctx.wizard.next();
  },
  (ctx) => {
    if (!(Number.isInteger(parseInt(ctx.message.text)))){
      ctx.reply('Input salah, silahkan ulang dari awal');
      return ctx.scene.leave();
    } else {
      orderObject.id = short.generate();
      orderObject.receiver_id = receivers[ctx.message.text-1].id;
      orderObject.receiver_name = receivers[ctx.message.text-1].name;
      orderObject.receiver_address = receivers[ctx.message.text-1].address;
      ctx.reply('Nama titik awal:')
      return ctx.wizard.next()
    }
  },
  (ctx) => {
    orderObject.origin = {}
    orderObject.origin.id = short.generate();
    orderObject.origin.name = ctx.message.text;
    ctx.reply('Alamat titik awal:')
    return ctx.wizard.next()
  },
  (ctx) => {
    orderObject.origin.address = ctx.message.text;
    ctx.reply('Lokasi titik awal:')
    return ctx.wizard.next()
  },
  (ctx) => {
    try{
      orderObject.origin.latitude = ctx.message.location.latitude;
      orderObject.origin.longitude = ctx.message.location.longitude;
    }
    catch(err) {
      ctx.reply('Lokasi titik awal salah, silahkan ulang dari awal')
      return ctx.scene.leave()
    }
    ctx.reply('Nama destinasi:')
    return ctx.wizard.next()
  },
  (ctx) => {
    orderObject.destination = {}
    orderObject.destination.id = short.generate();
    orderObject.destination.name = ctx.message.text;
    ctx.reply('Alamat destinasi:')
    return ctx.wizard.next()
  },
  (ctx) => {
    orderObject.destination.address = ctx.message.text;
    ctx.reply('Lokasi destinasi:')
    return ctx.wizard.next()
  },
  (ctx) => {
    try {
      orderObject.destination.latitude = ctx.message.location.latitude;
      orderObject.destination.longitude = ctx.message.location.longitude;
    } catch(err) {
      ctx.reply('Lokasi destinasi salah, silahkan ulang dari awal')
      return ctx.scene.leave()
    }
    ctx.reply('Deadline:')
    ctx.reply('*Tulis dalam bentuk format YYYY-MM-DD')
    return ctx.wizard.next()
  },
  (ctx) => {
    orderObject.deadline = ctx.message.text;
    ctx.reply('Jumlah Barang:')
    ctx.reply('*Masukkan jumlah barang pada pesanan ini')
    return ctx.wizard.next()
  },
  (ctx) => {
    numitem = ctx.message.text;
    orderObject.items = [];
    ctx.reply('Detail Barang, ketik apa saja untuk melanjutkan');
    return ctx.wizard.next()
  },
  (ctx) => {
    ctx.reply(`Barang #${itemincrement}`)
    ctx.reply('Nama Barang:');
    itemstartstep = ctx.wizard.cursor;
    return ctx.wizard.next()
  },
  (ctx) => {
    orderObject.items.push({name: ctx.message.text, qty: 0});
    ctx.reply('Kuantitas Barang:');
    return ctx.wizard.next()
  },
  (ctx) => {
    orderObject.items[orderObject.items.length-1].qty = ctx.message.text;
    numitem--;
    itemincrement++;
    if (numitem > 0){
      ctx.reply('Masukan barang selanjutnya, ketik apa saja untuk melanjutkan')
      return ctx.wizard.selectStep(itemstartstep);
    } else {
      itemincrement=1;
      ctx.reply('Input barang selesai, ketik apa saja untuk melanjutkan')
      return ctx.wizard.next()
    }
  },
  (ctx) => {
    let replymsg = 
    `Tambah Order Baru
    ========================

    ID: ${orderObject.id}

    Nama Pengirim: ${orderObject.sender_name}

    Alamat Pengirim: ${orderObject.sender_address}

    Nama Penerima: ${orderObject.receiver_name}

    Alamat Penerima: ${orderObject.receiver_address}

    Nama titik awal: ${orderObject.origin.name}

    Alamat titik awal: ${orderObject.origin.address}

    Nama destinasi: ${orderObject.destination.name}

    Alamat destinasi: ${orderObject.destination.address}

    Deadline: ${orderObject.deadline}
    
    Barang-barang:
    No. Nama       Kuantitas
    `
    orderObject.items.forEach((item, index) => {
      replymsg = replymsg + `${index+1}.      ${item.name}       ${item.qty}\n`
    });
    replymsg =replymsg + 'Apakah Anda mau menambahkan pesanan ini?';
    ctx.reply(
      replymsg
      ,Markup.inlineKeyboard([
        Markup.callbackButton('✅ Ya', 'neworder-yes'),
        Markup.callbackButton('❌ Tidak', 'neworder-no'),
      ]).extra())

    return ctx.scene.leave()
  }
)

//mockup truk
let trucksdata = []

let driversdata = []

let ordersdata = []

const addRouteScene = new WizardScene('add-route',
  (ctx) => {
    let trucktext = 'No.  Kode   Model  Kapasitas  Vendor  Seri\n'
    trucksdata.forEach((truck, index) => {
      trucktext = trucktext + `${index+1}.   ${truck.code}   ${truck.name} ${truck.capacity} ${truck.vendor} ${truck.series}\n`
    });
    ctx.reply('Truk:')
    ctx.reply(trucktext);
    ctx.reply(`Pilih dengan memasukkan angka, contoh: 1`)
    ctx.wizard.next();
  },
  (ctx) => {
    if (!(Number.isInteger(parseInt(ctx.message.text)))){
      ctx.reply('Input salah, silahkan ulang dari awal');
      return ctx.scene.leave();
    } else {
      routeObject.id = short.generate();
      routeObject.truck_id = trucksdata[ctx.message.text-1].id;
      routeObject.truck_code = trucksdata[ctx.message.text-1].code;
      routeObject.truck_model_name = trucksdata[ctx.message.text-1].name;
      let drivertext = 'No.  Nama Usia\n'
      driversdata.forEach((driver, index) => {
        drivertext = drivertext + `${index+1}.   ${driver.name}   ${driver.age}\n`
      });
      ctx.reply('Sopir:')
      ctx.reply(drivertext);
      ctx.reply(`Pilih dengan memasukkan angka, contoh: 1`)
      ctx.wizard.next();
    }
  },
  (ctx) => {
    if (!(Number.isInteger(parseInt(ctx.message.text)))){
      ctx.reply('Input salah, silahkan ulang dari awal');
      return ctx.scene.leave();
    } else {
    routeObject.driver_id = driversdata[ctx.message.text-1].id;
    routeObject.driver_name = driversdata[ctx.message.text-1].name;
    routeObject.driver_chat_id = driversdata[ctx.message.text-1].chat_id;
    let ordertext = 'Pilih Order\nNo.  Pengirim     Penerima     Awal      Destinasi\n'
      ordersdata.forEach((order, index) => {
        ordertext = ordertext + `${index+1}.   ${order.sender_name}   ${order.receiver_name}     ${order.origin_address} ${order.destination_address}\n`
      });
    ctx.reply(ordertext);
    ctx.reply('Order yang dipilih:')
    ctx.reply('*Masukkan angka dan dipisah dengan koma (,) jika lebih dari satu\n Contoh: 1,2,3\nMasukkan angka sesuai urutan rute yang akan ditempuh\nLalu tekan ENTER dan tulis OK untuk melanjutkan')
  }
  return ctx.wizard.next()
  },
  (ctx) => {
    let orderinput = ctx.message.text.split(',')
    orderinput.forEach((order, index) => {
      routepaths.push(
        {
          route_id: routeObject.id,
          order_id:ordersdata[order-1].id, 
          origin_id: ordersdata[order-1].origin_id,
          origin_address: ordersdata[order-1].origin_address,
          destination_id: ordersdata[order-1].destination_id,
          destination_address: ordersdata[order-1].destination_address,
          sequence: index+1
         });
    });
    routeObject.start_id = routepaths[0].origin_id;
    routeObject.finish_id = routepaths[routepaths.length-1].destination_id;
    return ctx.wizard.next()
  },
  (ctx) => {
    let replymsg = 
    `Tambah Rute Perjalanan Baru
    ========================

    ID: ${routeObject.id}

    Kode Truk : ${routeObject.truck_code},
    Model Truk: ${routeObject.truck_model_name}

    Sopir: ${routeObject.driver_name}

    Mulai: ${routepaths[0].origin_address}

    Akhir: ${routepaths[routepaths.length-1].destination_address}

    Rute yang ditempuh:
    No.  Awal   Tujuan
    `
    routepaths.forEach((order, index) => {
      replymsg = replymsg + `${index+1}.      ${order.origin_address}       ${order.destination_address}\n`
    });
    replymsg = replymsg + 'Apakah Anda mau membuat rute ini?';
    ctx.reply(
      replymsg
      ,Markup.inlineKeyboard([
        Markup.callbackButton('✅ Ya', 'newroute-yes'),
        Markup.callbackButton('❌ Tidak', 'newroute-no'),
      ]).extra())

    return ctx.scene.leave()
  }
)

let route_key = '';
const confirmRouteScene = new WizardScene('confirm-route',
  (ctx) => {
    let messageArray = ctx.message.text.split(' ');
    messageArray.shift();
    let id = messageArray[0];
    route_key = sha256(id);
    ctx.reply('Silahkan kirim QR Code Konfirmasi');
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    parseQRCode(url).then((res)=>{
      if (res === route_key) {
        let routeQuery = {
          type: 'insert',
          table: 'routes',
          values: {
            id: routeObject.id,
            driver_id: routeObject.driver_id,
            truck_id: routeObject.truck_id,
            start_id: routeObject.start_id,
            finish_id: routeObject.finish_id,
            status: 'ONGOING',
            num_route: routepaths.length 
          }
        }
        axios.request({
          baseURL:`http://${peers[peersindex]}`,
          url: '/createBlock/database',
          method: 'post',
          data: {block: {data:routeQuery}}
        }).then((res)=>{
          let routepathArray = []
          routepaths.forEach(path => {
            routepathArray.push({route_id: routeObject.id, order_id: path.order_id, origin_id: path.origin_id, destination_id: path.destination_id, seq: path.sequence, status: 'ONGOING'})
          });
          let routepathQuery = {
            type: 'insert',
            table: 'route_paths',
            values: routepathArray
          }
          axios.request({
            baseURL:`http://${peers[peersindex]}`,
            url: '/createBlock/database',
            method: 'post',
            data: {block: {data:routepathQuery}}
          }).then(()=>{
            let dateInput = getCurrentTimestamp()
            axios.request({
              baseURL:`http://${peers[peersindex]}`,
              url: '/newblockchain',
              method: 'post',
              data: {id:routeObject.id, data: {order_id: routeObject.order_id, origin_address: routepaths[0].origin_address, destination_address: routepaths[routepaths.length-1].destination_address, status: 'ONGOING', date:dateInput}}
            }).then(()=>{
              QRCode.toFile('./public/images/newroute.png', routeObject.id, ()=> {
                ctx.reply('Konfirmasi diterima, berikut QR Code dari rute');
                ctx.telegram.sendPhoto(routeObject.driver_chat_id, {source: './public/images/newroute.png'});
                ctx.telegram.sendMessage(pt_id, 'Konfirmasi dari sopir diterima, pembuatan rute baru selesai')
                ctx.telegram.sendPhoto(pt_id, {source: './public/images/newroute.png'});
                routeObject = {}
                routepaths = []
              })
            }).catch((error)=> {
              ctx.reply('Server error, silahkan coba lagi nanti')
            })
          }).catch((error)=> {
            ctx.reply('Server error, silahkan coba lagi nanti')
          })
       }).catch((error)=> {
            ctx.reply('Server error, silahkan coba lagi nanti')
      })
      } else {
        ctx.reply('QR Code ditolak coba ulangi lagi konfirmasi')
      }
      return ctx.scene.leave()
    })
  })
})

let waybill_key = ''

const confirmDriverWaybillScene = new WizardScene('confirm-driver-waybill',
(ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  let id = messageArray[0];
  waybill_key = sha256(id);
  ctx.reply('Silahkan kirim QR Code Konfirmasi');
  ctx.wizard.next();
},
(ctx) => {
  let file_id = ctx.message.photo[0].file_id
  ctx.telegram.getFileLink(file_id).then((url)=>{
  parseQRCode(url).then((res)=>{
    if (res === waybill_key) {
      waybillObject.confirm_driver = true
      ctx.telegram.sendMessage(pt_id, `Konfirmasi dari sopir diterima untuk waybill baru`);
      if (waybillObject.confirm_sender && waybillObject.confirm_receiver) {
        ctx.scene.enter('waybill-confirmed')
      } 
    } else {
      ctx.reply('QR Code ditolak coba ulangi lagi konfirmasi')
    }
  return ctx.scene.leave()
  })
})
})
//mockup data for waybill
let waybilldata = {} 

let waybilltext = ''
const issueWaybillScene = new WizardScene('issue-waybill',
  (ctx) => {
      waybillObject= waybilldata
      waybillObject.id = short.generate()
      let replymsg = 
    `Waybill Baru
    ========================

    ID: ${waybillObject.id}

    Kode Truk : ${waybillObject.truck_code},

    Sopir: ${waybillObject.driver_name}

    Pengirim: ${waybillObject.sender_name},

    Titik Awal: ${waybillObject.origin_address},

    Penerima: ${waybillObject.receiver_name},

    Titik Akhir: ${waybillObject.destination_address}

    `
      ctx.reply(replymsg)
      waybilltext = replymsg;
      ctx.reply('Masukkan Tanggal Pengiriman (YYYY-MM-DD):');
    
    ctx.wizard.next()
  },
  (ctx) => {
    waybillObject.date_send = ctx.message.text;
    waybillObject.confirm_sender = false;
    waybillObject.confirm_receiver=false;
    waybillObject.confirm_driver=false;
    waybilltext = waybilltext + `Tanggal Pengiriman: ${ctx.message.text}\n`
    let replymsg = waybilltext + 'Apakah Anda mau membuat waybill ini?';
    ctx.reply(
      replymsg
      ,Markup.inlineKeyboard([
        Markup.callbackButton('✅ Ya', 'waybill-yes'),
        Markup.callbackButton('❌ Tidak', 'waybill-no'),
      ]).extra())
  }
)

const waybilConfirmedScene = new Scene('waybill-confirmed')
waybilConfirmedScene.enter((ctx) => {
  let waybillQuery = {
    type: 'insert',
    table: 'waybills',
    values: {
      id: waybillObject.id,
      order_id: waybillObject.order_id,
      truck_id: waybillObject.truck_id,
      driver_id: waybillObject.driver_id,
      sender_id: waybillObject.sender_id,
      receiver_id: waybillObject.receiver_id,
      date_send: waybillObject.date_send,
      status: 'NOT PICKED'
    }
  }
  axios.request({
    baseURL:`http://${peers[peersindex]}`,
    url: '/createBlock/database',
    method: 'post',
    data: {block: {data:waybillQuery}}
  }).then(()=>{
    let dateInput = getCurrentTimestamp();
    axios.request({
      baseURL:`http://${peers[peersindex]}`,
      url: '/newblockchain',
      method: 'post',
      data: {id:waybillObject.id, data: {status: 'NOT PICKED', date:dateInput }, initContract: true, waybillData: waybillObject }
    }).then(()=>{
      QRCode.toFile('./public/images/waybill.png', waybillObject.id, ()=> {
        ctx.telegram.sendMessage(pt_id, `Waybill ${waybillObject.id} telah aktif, berikut QR Code dari waybill`);
        ctx.telegram.sendMessage(waybillObject.driver_chat_id, `Waybill ${waybillObject.id} telah aktif, berikut QR Code dari waybill`);
        ctx.telegram.sendMessage(waybillObject.sender_chat_id, `Waybill ${waybillObject.id} telah aktif, berikut QR Code dari waybill`);
        ctx.telegram.sendMessage(waybillObject.receiver_chat_id, `Waybill ${waybillObject.id} telah aktif, berikut QR Code dari waybill`);
        ctx.telegram.sendPhoto(pt_id, {source: './public/images/waybill.png'});
        ctx.telegram.sendPhoto(waybillObject.driver_chat_id, {source: './public/images/waybill.png'});
        ctx.telegram.sendPhoto(waybillObject.sender_chat_id, {source: './public/images/waybill.png'});
        ctx.telegram.sendPhoto(waybillObject.receiver_chat_id, {source: './public/images/waybill.png'});
        waybillObject = {};
        ctx.scene.leave();
      })
    }).catch((error)=> {
      ctx.reply('Server error, silahkan coba lagi nanti')
    })
  }).catch((error)=> {
    ctx.reply('Server error, silahkan coba lagi nanti')
  })
})
let nik = '';

const securityCheckScene = new WizardScene('security-check',
  (ctx) => {
    let messageArray = ctx.message.text.split(' ');
    messageArray.shift();
    nik = messageArray[0];
    let id = messageArray[1];
    waybill_key = sha256(id)+nik;
    ctx.reply('Silahkan kirim QR Code Konfirmasi dari driver');
    db.query({
      sql: 'SELECT `chat_id` FROM `drivers` WHERE nik = ?',
      timeout: 40000, // 40s
      values: [nik]
    }, function(error, results, fields) {
        let driver_id = results[0].chat_id;
        ctx.telegram.sendMessage(driver_id, 'Pengecekan Keamanan, silahkan login ke web untuk memperoleh QR Code');
      if (error){
        console.log("ERROR", error.sqlMessage)
      }
    });
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    parseQRCode(url).then((res)=>{
      let check_key = res+nik;
      if (check_key === waybill_key) {
        ctx.reply('Sopir terkonfirmasi')
      } else {
        ctx.reply('QR Code ditolak, coba lagi dan periksa identitas sopir!')
      }
      return ctx.scene.leave()
    })
  })
})

let load_id = ''
const confirmDriverLoadScene = new WizardScene('confirm-load-driver',
  (ctx) => {
    let messageArray = ctx.message.text.split(' ');
    messageArray.shift();
    let id = messageArray[0];
    load_id = id;
    waybill_key = sha256(id);
    ctx.reply('Silahkan kirim QR Code Konfirmasi');
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    parseQRCode(url).then((res)=>{
      if (res === waybill_key) {
        let loadQuery = {
          type: 'update',
          table: 'waybills',
          modifier: {
            status: 'PICKED'
          },
          condition: {
            id: load_id
          }
        }
        axios.request({
          baseURL:`http://${peers[peersindex]}`,
          url: '/createBlock/database',
          method: 'post',
        data: {block: {data:loadQuery}}
        }).then((res)=>{
          let dateInput = getCurrentTimestamp();
          axios.request({
            baseURL:`http://${peers[peersindex]}`,
            url: `/createBlock/${load_id}`,
            method: 'post',
            data: {block: {data:{status: 'PICKED', date:dateInput}}}
          }).then(()=>{
              db.query({
                sql: 'SELECT p.chat_id, w.order_id FROM waybills w inner join users p on w.sender_id = p.id  WHERE w.id = ?',
                timeout: 40000, // 40s
                values: [load_id]
              }, function(error, results, fields) {
                  let produsen_id = results[0].chat_id;
                  let order_id = results[0].order_id;
                  let loadOrderQuery = {
                    type: 'update',
                    table: 'orders',
                    modifier: {
                      status: 'PICKED'
                    },
                    condition: {
                      id: order_id
                    }
                  }
                  axios.request({
                    baseURL:`http://${peers[peersindex]}`,
                    url: '/createBlock/database',
                    method: 'post',
                  data: {block: {data:loadOrderQuery}}
                  }).then((res)=>{
                    dateInput = getCurrentTimestamp();
                    axios.request({
                      baseURL:`http://${peers[peersindex]}`,
                      url: `/createBlock/${order_id}`,
                      method: 'post',
                      data: {block: {data:{status: 'PICKED', payment_status: 'UNPAID', date:dateInput}}}
                    }).then(()=>{
                      ctx.reply('Konfirmasi diterima');
                      ctx.telegram.sendMessage(produsen_id, `Konfirmasi pemuatan untuk waybill ${load_id} dari sopir diterima`);
                      ctx.telegram.sendMessage(pt_id, `Konfirmasi pemuatan untuk waybill ${load_id} dari sopir diterima`);
                    }).catch((error)=> {
                      ctx.reply('Server error, silahkan coba lagi nanti')
                    })
                  }).catch((error)=> {
                    ctx.reply('Server error, silahkan coba lagi nanti')
                  })
                if (error){
                  console.log("ERROR", error.sqlMessage)
                }
              });
            }).catch((error)=> {
              ctx.reply('Server error, silahkan coba lagi nanti')
            })
          }).catch((error)=> {
            ctx.reply('Server error, silahkan coba lagi nanti')
          })
      } else {
        ctx.reply('QR Code ditolak coba ulangi lagi konfirmasi')
      }
      return ctx.scene.leave()
    })
  })
})

let unload_id = ''
const confirmDriverUnloadScene = new WizardScene('confirm-unload-driver',
(ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  let id = messageArray[0];
  unload_id = id;
  waybill_key = sha256(id);
  ctx.reply('Silahkan kirim QR Code Konfirmasi');
  ctx.wizard.next();
},
(ctx) => {
  let file_id = ctx.message.photo[0].file_id
  ctx.telegram.getFileLink(file_id).then((url)=>{
  parseQRCode(url).then((res)=>{
    if (res === waybill_key) {
      let unloadQuery = {
        type: 'update',
        table: 'waybills',
        modifier: {
          status: 'DELIVERED'
        },
        condition: {
          id: unload_id
        }
      }
      axios.request({
        baseURL:`http://${peers[peersindex]}`,
        url: '/createBlock/database',
        method: 'post',
      data: {block: {data:unloadQuery}}
      }).then((res)=>{
        let dateInput = getCurrentTimestamp();
        axios.request({
          baseURL:`http://${peers[peersindex]}`,
          url: `/createBlock/${unload_id}`,
          method: 'post',
          data: {block: {data:{status: 'DELIVERED', date:dateInput}}}
        }).then(()=>{
            db.query({
              sql: 'SELECT p.chat_id, o.chat_id, w.order_id, w.truck_id, rp.route_id FROM waybills w inner join users p on w.receiver_id = p.id inner join users o on w.sender_id=o.id inner join route_paths rp on w.order_id=rp.order_id  WHERE w.id = ?',
              timeout: 40000, // 40s
              values: [unload_id]
            }, function(error, results, fields) {
                let konsumen_id = results[0].chat_id;
                let produsen_id = results[0].chat_id;
                let order_id = results[0].order_id;
                let truck_id = results[0].truck_id;
                let route_id = results[0].route_id;
                let unloadOrderQuery = {
                  type: 'update',
                  table: 'orders',
                  modifier: {
                    status: 'COMPLETED'
                  },
                  condition: {
                    id: order_id
                  }
                }
                axios.request({
                  baseURL:`http://${peers[peersindex]}`,
                  url: '/createBlock/database',
                  method: 'post',
                data: {block: {data:unloadOrderQuery}}
                }).then((res)=>{
                  dateInput = getCurrentTimestamp();
                  axios.request({
                    baseURL:`http://${peers[peersindex]}`,
                    url: `/createBlock/${order_id}`,
                    method: 'post',
                    data: {block: {data:{status: 'COMPLETED', payment_status: 'UNPAID', date:dateInput}}}
                  }).then(()=>{
                    let routePathQuery = {
                      type: 'update',
                      table: 'route_paths',
                      modifier: {
                        status: 'COMPLETED'
                      },
                      condition: {
                        order_id: order_id
                      }
                    }
                    axios.request({
                      baseURL:`http://${peers[peersindex]}`,
                      url: '/createBlock/database',
                      method: 'post',
                    data: {block: {data:routePathQuery}}
                    }).then(()=>{
                      db.query({
                        sql: 'select distinct rp.seq, r.num_route, rop.address origin_address, rdp.address destination_address from route_paths rp inner join routes r inner join points rop on rp.origin_id=rop.id inner join points rdp on rp.destination_id=rdp.id where rp.order_id = ? AND rp.route_id= ?;',
                        timeout: 40000, // 40s
                        values: [order_id, route_id]
                      }, function(error, results, fields) {
                        let num_route = results[0].num_route;
                        let seq = results[0].seq;
                        let origin_address = results[0].origin_address;
                        let destination_address = results[0].destination_address;
                        axios.request({
                          baseURL:`http://${peers[peersindex]}`,
                          url: `/createBlock/${route_id}`,
                          method: 'post',
                          data: {block: {data: {order_id: order_id, origin_address: origin_address, destination_address: destination_address, status: 'COMPLETED', date:dateInput}}}
                        }).then(()=>{
                          if (num_route == seq) {
                            //update route and truck histories
                            let routeQuery = {
                              type: 'update',
                              table: 'routes',
                              modifier: {
                                status: 'COMPLETED'
                              },
                              condition: {
                                id: route_id
                              }
                            }
                            axios.request({
                              baseURL:`http://${peers[peersindex]}`,
                              url: '/createBlock/database',
                              method: 'post',
                            data: {block: {data:routeQuery}}
                            }).then(()=>{
                              axios.request({
                                baseURL:`http://${peers[peersindex]}`,
                                url: `/createBlock/${truck_id}`,
                                method: 'post',
                                data: {block: {data: {route_id: route_id, status: 'COMPLETED', date:dateInput}}}
                              })
                          }).catch((error)=> {
                            ctx.reply('Server error, silahkan coba lagi nanti')
                            })
                        }
                        setTimeout(() => {
                          axios.request({
                            baseURL:`http://${peers[peersindex]}`,
                            url: `/blockchain/${unload_id}`,
                            method: 'get',
                          }).then((res)=>{
                            let chain = res.data.chain;
                            let firstblock = chain[0];
                            let paymentAmount = firstblock.data.data.paymentAmount;
                            ctx.reply('Konfirmasi diterima');
                            ctx.telegram.sendMessage(konsumen_id, `Konfirmasi bongkar muatan untuk waybill ${unload_id} dari sopir diterima`);
                            ctx.telegram.sendMessage(pt_id, `Konfirmasi bongkar muatan untuk waybill ${unload_id} dari sopir diterima`);  
                            ctx.telegram.sendMessage(produsen_id, `Waybill ${unload_id} untuk pesanan ${order_id} telah selesai diantarkan, silahkan membayar biaya transportasi sejumlah Rp${paymentAmount} kemudian lakukan konfirmasi pembayaran dengan perintah /confirmpayment`)
                          })
                          
                        }, 1000)
                  
                      }).catch((error)=> {
                        ctx.reply('Server error, silahkan coba lagi nanti')
                        })
                      if (error){
                        console.log("ERROR", error.sqlMessage)
                      }
                    });
                  }).catch((error)=> {
                    ctx.reply('Server error, silahkan coba lagi nanti')
                    })
                  }).catch((error)=> {
                    ctx.reply('Server error, silahkan coba lagi nanti')
                    })
                  }).catch((error)=> {
                    ctx.reply('Server error, silahkan coba lagi nanti')
                  })
                  if (error){
                    console.log("ERROR", error.sqlMessage)
                  }
                });
            }).catch((error)=> {
              ctx.reply('Server error, silahkan coba lagi nanti')
              })
          }).catch((error)=> {
            ctx.reply('Server error, silahkan coba lagi nanti')
            })
        } else {
      ctx.reply('QR Code ditolak coba ulangi lagi konfirmasi')
    }
    return ctx.scene.leave()
  })
})
})

//mockup history truck
let truckHistoryData = []

const truckHistoryScene = new WizardScene('history-truck',
  (ctx) => {
    ctx.reply('Silahkan kirim QR Code truk yang ingin dilacak');
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    parseQRCode(url).then((res)=>{
      let truck_id = res;
      axios.request({
        baseURL:`http://${peers[peersindex]}`,
        url: `/blockchain/${res}`,
        method: 'get',
      }).then((res)=>{
        let chain = res.data.chain;
        chain.forEach(block => {
          truckHistoryData.push({route_id: block.data.data.route_id, status:block.data.data.status, date: block.data.data.date })
        });
        let replymsg =`
        Riwayat Truk ${truck_id}\n
        No. ID       Rute   Status     Tanggal\n`
        truckHistoryData.forEach((history, index) => {
          replymsg = replymsg + `${index+1}.      ${history.route_id}       ${history.status}      ${history.date}\n`
        });
        ctx.reply('Posisi saat ini');
        ctx.reply(replymsg)
        firebase.database().ref(`/trucks/${truck_id}`).once('value').then((snapshot) => {
          let location = snapshot.val()
          ctx.replyWithLocation(location.lat, location.long)
          truckHistoryData = []
          return ctx.scene.leave();
        }).catch((error)=>{
          ctx.reply('Lokasi tidak ditemukan')
          truckHistoryData = []
          return ctx.scene.leave();
        })
      })
      
    })
  })
})


//mockup history pesanan
let orderHistoryData = []

const orderHistoryScene = new WizardScene('history-order',
  (ctx) => {
    ctx.reply('Silahkan kirim QR Code pesanan yang ingin dilacak');
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    parseQRCode(url).then((res)=>{
      let order_id = res;
      axios.request({
        baseURL:`http://${peers[peersindex]}`,
        url: `/blockchain/${res}`,
        method: 'get',
      }).then((res)=>{
        let chain = res.data.chain;
        chain.forEach(block => {
          orderHistoryData.push({status:block.data.data.status, payment_status:block.data.data.payment_status, date: block.data.data.date })
        });
        let replymsg =`
        Riwayat Pesanan ${order_id}\n
        No. Status   Payment     Tanggal\n`
        orderHistoryData.forEach((history, index) => {
          replymsg = replymsg + `${index+1}      ${history.status}   ${history.payment_status}     ${history.date}\n`
        });
        ctx.reply(replymsg)
        orderHistoryData = [];
        return ctx.scene.leave()
      })
   })
  })
})

//mockup history pesanan
let routeHistoryData = []

const routeHistoryScene = new WizardScene('history-route',
  (ctx) => {
    ctx.reply('Silahkan kirim QR Code rute perjalanan yang ingin dilacak');
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    parseQRCode(url).then((res)=>{
      let route_id = res;
      axios.request({
        baseURL:`http://${peers[peersindex]}`,
        url: `/blockchain/${res}`,
        method: 'get',
      }).then((res)=>{
        let chain = res.data.chain;
        chain.forEach(block => {
          routeHistoryData.push({order_id:block.data.data.order_id, origin_address:block.data.data.origin_address, destination_address:block.data.data.destination_address, status:block.data.data.status, date: block.data.data.date })
      })
      let replymsg =`
      Riwayat Rute ${route_id}\n
      No.      ID Order    Awal       Tujuan       Status     Tanggal\n`
      routeHistoryData.forEach((history, index) => {
        replymsg = replymsg + `${index+1}      ${history.order_id}    ${history.origin_address}    ${history.destination_address}   ${history.status}   ${history.date}\n`
      });
      ctx.reply(replymsg)
      ctx.reply('Posisi saat ini')
      db.query({
        sql: 'SELECT truck_id FROM routes WHERE id = ?',
        timeout: 40000, // 40s
        values: [route_id]
      }, function(error, results, fields) {
          let truck_id = results[0].truck_id;
          firebase.database().ref(`/trucks/${truck_id}`).once('value').then((snapshot) => {
            let location = snapshot.val()
            ctx.replyWithLocation(location.lat, location.long)
            routeHistoryData = []
            return ctx.scene.leave();
          }).catch((error)=>{
            ctx.reply('Lokasi tidak ditemukan')
            routeHistoryData = [];
            return ctx.scene.leave();
          })
          if (error){
          console.log("ERROR", error.sqlMessage)
        }
      });
    })
  })
})
})

//mockup history pesanan
let waybillHistoryData = []

const waybillHistoryScene = new WizardScene('history-waybill',
  (ctx) => {
    ctx.reply('Silahkan kirim QR Code waybill yang ingin dilacak');
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    parseQRCode(url).then((res)=>{
      let waybill_id = res;
      axios.request({
        baseURL:`http://${peers[peersindex]}`,
        url: `/blockchain/${res}`,
        method: 'get',
      }).then((res)=>{
        let chain = res.data.chain;
        chain.forEach(block => {
          waybillHistoryData.push({status:block.data.data.status, date: block.data.data.date })
      })
      let replymsg =`
      Riwayat Waybill ${waybill_id}\n
      No.     Status     Tanggal\n`
      waybillHistoryData.forEach((history, index) => {
        replymsg = replymsg + `${index+1}      ${history.status}   ${history.date}\n`
      });
      ctx.reply(replymsg)
      ctx.reply('Posisi saat ini')
      db.query({
        sql: 'SELECT truck_id FROM waybills WHERE id = ?',
        timeout: 40000, // 40s
        values: [waybill_id]
      }, function(error, results, fields) {
          let truck_id = results[0].truck_id;
          firebase.database().ref(`/trucks/${truck_id}`).once('value').then((snapshot) => {
            let location = snapshot.val()
            ctx.replyWithLocation(location.lat, location.long)
            waybillHistoryData = []
            return ctx.scene.leave();
          }).catch((error)=>{
            ctx.reply('Lokasi tidak ditemukan')
            waybillHistoryData = [];
            return ctx.scene.leave();
          })
          if (error){
          console.log("ERROR", error.sqlMessage)
        }
      });
    })
  })
  })
})

const confirmPaymentScene = new WizardScene('confirm-payment',
  (ctx) => {
    ctx.reply('ID Order untuk pembayaran');
    ctx.wizard.next();
  },
  (ctx) => {
    paymentObject.order_id = ctx.message.text;
    ctx.reply('Upload Foto Bukti Pembayaran')
    ctx.wizard.next();
  },
  (ctx) => {
    let file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then((url)=>{
    ctx.telegram.sendMessage(pt_id, `Bukti pembayaran untuk pesanan ${paymentObject.order_id}
    Silahkan konfirmasi dengan perintah /receivepayment SPASI {ID Order}`);
    ctx.telegram.sendPhoto(pt_id, {url: url} )
    return ctx.scene.leave()
  })
})

// Create scene manager
const stage = new Stage()
stage.command('cancel', leave())

// Scene registration
stage.register(userRegistrationScene)
stage.register(driverRegistrationScene)
stage.register(addTruckScene)
stage.register(addTruckModelScene)
stage.register(addOrderScene)
stage.register(addRouteScene)
stage.register(confirmRouteScene)
stage.register(issueWaybillScene)
stage.register(confirmDriverWaybillScene)
stage.register(securityCheckScene)
stage.register(confirmDriverLoadScene)
stage.register(confirmDriverUnloadScene)
stage.register(truckHistoryScene)
stage.register(orderHistoryScene)
stage.register(routeHistoryScene)
stage.register(waybillHistoryScene)
stage.register(confirmPaymentScene)
stage.register(waybilConfirmedScene)


stage.action('newtruck-yes', (ctx)=>{
let truckQuery = {
    type: 'insert',
    table: 'trucks',
    values: {
      id: truckObject.id,
      model_id: truckObject.model_id,
      code: truckObject.code,
      vendor: truckObject.vendor,
      series: truckObject.series
    }
  }
  axios.request({
    baseURL:`http://${peers[peersindex]}`,
    url: '/createBlock/database',
    method: 'post',
  data: {block: {data:truckQuery}}
  }).then((res)=>{
    let dateInput = getCurrentTimestamp();
    axios.request({
      baseURL:`http://${peers[peersindex]}`,
      url: '/newblockchain',
      method: 'post',
    data: {id:truckObject.id, data: {route_id: '0', status: 'created', date:dateInput}}
    }).then(()=>{
      db.query({
        sql: 'SELECT `id` FROM `trucks` WHERE id = ?',
        timeout: 40000, // 40s
        values: [truckObject.id]
      }, function(error, results, fields) {
          let truck_id = results[0].id;
          QRCode.toFile('./public/images/newtruck.png', truck_id, ()=> {
            
            ctx.reply('Input Data Truk Selesai')
            ctx.telegram.sendPhoto(pt_id, {source: './public/images/newtruck.png'});
            truckObject = {}
          })
        if (error){
          console.log("ERROR", error.sqlMessage)
        }
      });
      
    }).catch((error)=> {
      ctx.reply('Server error, silahkan coba lagi nanti')
    })
    })
  .catch((error)=> {
    ctx.reply('Server error, silahkan coba lagi nanti')
  })
})

stage.action('newtruck-no', (ctx)=>{
  truckObject = {}
  ctx.reply('Input Data Truk Batal')
})


stage.action('newmodeltruck-yes', (ctx)=>{
  let modelQuery = {
    type: 'insert',
    table: 'truck_models',
    values: {
      id: truckModelObject.id,
      name: truckModelObject.name,
      capacity: truckModelObject.capacity,
    }
  }
  axios.request({
    baseURL:`http://${peers[peersindex]}`,
    url: '/createBlock/database',
    method: 'post',
    data: {block: {data:modelQuery}}
  }).then((res)=>{
    ctx.reply('Input Data Model Truk Selesai')
    truckModelObject = {}
    })
  .catch((error)=> {
    ctx.reply('Server error, silahkan coba lagi nanti')
  })
})

stage.action('newmodeltruck-no', (ctx)=>{
  truckModelObject = {}
  ctx.reply('Input Data Model Truk Batal')
})


stage.action('neworder-yes', (ctx)=>{
  let pointQuery = {
    type: 'insert',
    table: 'points',
    values: [
      {id: orderObject.origin.id, name: orderObject.origin.name, address: orderObject.origin.address, latitude: orderObject.origin.latitude, longitude: orderObject.origin.longitude},
      {id: orderObject.destination.id, name: orderObject.destination.name, address: orderObject.destination.address, latitude: orderObject.destination.latitude, longitude: orderObject.destination.longitude},
    ]
  }
  axios.request({
    baseURL:`http://${peers[peersindex]}`,
    url: '/createBlock/database',
    method: 'post',
    data: {block: {data:pointQuery}}
  }).then(()=> {
    let orderQuery = {
      type: 'insert',
      table: 'orders',
      values: {
        id: orderObject.id,
        sender_id: orderObject.sender_id,
        receiver_id: orderObject.receiver_id,
        origin_id: orderObject.origin.id,
        destination_id: orderObject.destination.id,
        deadline: orderObject.deadline,
        status: 'ONGOING', 
        payment_status: 'UNPAID'
      }
    }
    axios.request({
      baseURL:`http://${peers[peersindex]}`,
      url: '/createBlock/database',
      method: 'post',
      data: {block: {data:orderQuery}}
    }).then((res)=>{
      let itemArray = []
      orderObject.items.forEach(item => {
        itemArray.push({order_id: orderObject.id, item_name: item.name, qty: item.qty})
      });
      let itemQuery = {
        type: 'insert',
        table: 'order_items',
        values: itemArray
      }
      axios.request({
        baseURL:`http://${peers[peersindex]}`,
        url: '/createBlock/database',
        method: 'post',
        data: {block: {data:itemQuery}}
      }).then(()=>{
      let dateInput = getCurrentTimestamp();
        axios.request({
          baseURL:`http://${peers[peersindex]}`,
          url: '/newblockchain',
          method: 'post',
          data: {id:orderObject.id, data: {status: 'ONGOING', payment_status: 'UNPAID', date:dateInput}}
        }).then(()=>{
          QRCode.toFile('./public/images/neworder.png', orderObject.id, ()=> {
            let ordermsg =`
            Order Baru Dibuat
            ========================
        
            ID: ${orderObject.id}
        
            Nama Pengirim: ${orderObject.sender_name}
        
            Alamat Pengirim: ${orderObject.sender_address}
        
            Nama Penerima: ${orderObject.receiver_name}
        
            Alamat Penerima: ${orderObject.receiver_address}
        
            Nama titik awal: ${orderObject.origin.name}
        
            Alamat titik awal: ${orderObject.origin.address}
        
            Nama destinasi: ${orderObject.destination.name}
        
            Alamat destinasi: ${orderObject.destination.address}
        
            Deadline: ${orderObject.deadline}
            `
            orderObject.items.forEach((item, index) => {
              ordermsg = ordermsg + `${index+1}.      ${item.name}       ${item.qty}\n`
            });
            ctx.telegram.sendMessage( pt_id, ordermsg)
            ctx.telegram.sendPhoto(pt_id, {source: './public/images/neworder.png'});
            ctx.telegram.sendPhoto(orderObject.sender_chat_id, {source: './public/images/neworder.png'});
            ctx.reply('Input Data Pesanan Selesai')
            orderObject = {}
          })
        }).catch((error)=> {
          ctx.reply('Server error, silahkan coba lagi nanti')
        })
      }).catch((error)=> {
        ctx.reply('Server error, silahkan coba lagi nanti')
      })
    }).catch((error)=> {
      ctx.reply('Server error, silahkan coba lagi nanti')
    })
  }).catch((error)=> {
    ctx.reply('Server error, silahkan coba lagi nanti')
  })
  
  
})

stage.action('neworder-no', (ctx)=>{
  orderObject = {}
  
  ctx.reply('Input Data Pesanan Batal')
})

stage.action('newroute-yes', (ctx)=>{
    let replymsg = 
    `Konfirmasi Rute Perjalanan Baru
    ========================

    ID: ${routeObject.id}

    Kode Truk : ${routeObject.truck_code},
    Model Truk: ${routeObject.truck_model_name}

    Sopir: ${routeObject.driver_name}

    Mulai: ${routepaths[0].origin_address}

    Akhir: ${routepaths[routepaths.length-1].destination_address}

    Rute yang ditempuh:
    No.  Awal   Tujuan
    `
    routepaths.forEach((order, index) => {
      replymsg = replymsg + `${index+1}.      ${order.origin_address}       ${order.destination_address}\n`
    });
    ctx.reply('Menunggu konfirmasi dari sopir');
    ctx.telegram.sendMessage( routeObject.driver_chat_id, replymsg)
    ctx.telegram.sendMessage( routeObject.driver_chat_id, 'Silahkan konfirmasi dengan mengirim QR Code yang diperoleh di Web')
    ctx.telegram.sendMessage( routeObject.driver_chat_id, 'Lakukan konfirmasi dengan perintah /confirmroute SPASI {ID rute} lalu kirimkan QR Code')
  })
  

stage.action('newroute-no', (ctx)=>{
  routeObject = {}
  routepaths = []
  ctx.reply('Input Rute Baru Batal')
})

stage.action('waybill-yes', (ctx)=> {
  let drivermsg = waybilltext + `Silahkan melakukan konfirmasi dengan perintah /confirmdriverwaybill SPASI {ID Waybill}`
    let sendermsg = waybilltext + `Silahkan melakukan konfirmasi dengan perintah /confirmsenderwaybill SPASI {ID Waybill}`
    let receivermsg = waybilltext + `Silahkan melakukan konfirmasi dengan perintah /confirmreceiverwaybill SPASI {ID Waybill}`
    ctx.telegram.sendMessage(pt_id, 'Waybill dibuat, menunggu konfirmasi seluruh pihak yang terlibat');
    ctx.telegram.sendMessage(waybillObject.driver_chat_id, drivermsg);
    ctx.telegram.sendMessage(waybillObject.sender_chat_id, sendermsg);
    ctx.telegram.sendMessage(waybillObject.receiver_chat_id, receivermsg);
})

stage.action('waybill-no', (ctx)=>{
  waybillObject = {}
  ctx.reply('Input Waybill Baru Batal')
})

bot.use(session())
bot.use(stage.middleware())

//bot actions

bot.action('register', (ctx) => {
  ctx.reply('Pilih Jenis Pengguna', Markup.inlineKeyboard([
    Markup.callbackButton('Produsen', 'produsen'),
    Markup.callbackButton('Konsumen', 'konsumen'),
    Markup.callbackButton('Sopir Truk', 'sopir')
  ]).extra())
})

bot.action('produsen', (ctx)=> {
  ctx.telegram.sendMessage(pt_id, `Produsen baru melakukan registrasi`)
  produsen_id = ctx.chat.id;
  ctx.scene.enter('user-registration')
})

bot.action('konsumen', (ctx)=> {
  ctx.telegram.sendMessage(pt_id, `Konsumen baru melakukan registrasi`)
  konsumen_id = ctx.chat.id;
  ctx.scene.enter('user-registration')
})

bot.action('sopir', (ctx)=> {
  ctx.telegram.sendMessage(pt_id, `Sopir baru melakukan registrasi`)
  driver_id = ctx.chat.id;
  ctx.scene.enter('driver-registration')
})

bot.action('truck-history', (ctx)=> {
  ctx.scene.enter('history-truck')
})

bot.action('order-history', (ctx)=> {
  ctx.scene.enter('history-order')
})

bot.action('route-history', (ctx)=> {
  ctx.scene.enter('history-route')
})

bot.action('waybill-history', (ctx)=> {
  ctx.scene.enter('history-waybill')
})

//bot commands

//registration
bot.command('regdriver', (ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray[0];
  const userdata = messageArray[1].split('#');
  const username = userdata[0];
  const password = userdata[1];
  regObject.username = username;
  regObject.password = sha256(password);
  let regQuery = {
    type: 'insert',
    table: 'drivers',
    values: {
      id: regObject.id,
      name: regObject.name,
      age: regObject.age,
      phone: regObject.phone,
      nik: regObject.nik,
      username: regObject.username,
      password: regObject.password,
      chat_id: regObject.chat_id,
    }
  }
  axios.request({
    baseURL:`http://${peers[peersindex]}`,
    url: '/createBlock/database',
    method: 'post',
    data: {block: {data:regQuery}}
  }).then((res)=>{
      db.query({
        sql: 'SELECT `chat_id` FROM `drivers` WHERE id = ?',
        timeout: 40000, // 40s
        values: [regObject.id]
      }, function(error, results, fields) {
          let driver_id = results[0].chat_id;
          regObject = {};
          ctx.telegram.sendMessage(driver_id, `Registrasi telah dikonfirmasi`)
          ctx.reply(`Konfirmasi sopir ${id} sukses`);
          regObject = {};
          ctx.telegram.sendMessage(driver_id, 
            `Registrasi telah dikonfirmasi
            
            username: ${username}
            password: ${password}
            
            PESAN INI AKAN TERHAPUS SETELAH 10 DETIK
            `
            ).then((msg) => {
              setTimeout(() =>{
                ctx.telegram.deleteMessage(driver_id, msg.message_id);
              },10000)
            })
        if (error){
          console.log("ERROR", error.sqlMessage)
        }
      });
    })
  .catch((error)=> {
    ctx.reply('Server error, silahkan coba lagi nanti')
  })
})

bot.command('reguser', (ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray.join();
  let regQuery = {
    type: 'insert',
    table: 'users',
    values: {
      id: regObject.id,
      name: regObject.name,
      address: regObject.address,
      phone: regObject.phone,
      chat_id: regObject.chat_id
    }
  }
  axios.request({
    baseURL:`http://${peers[peersindex]}`,
    url: '/createBlock/database',
    method: 'post',
    data: {block: {data:regQuery}}
  }).then(()=>{
    db.query({
      sql: 'SELECT `chat_id` FROM `users` WHERE id = ?',
      timeout: 40000, // 40s
      values: [regObject.id]
    }, function(error, results, fields) {
        let user_id = results[0].chat_id;
        regObject = {};
        ctx.reply(`Konfirmasi pengguna ${id} sukses`);
        ctx.telegram.sendMessage(user_id, `Registrasi telah dikonfirmasi`)
      if (error){
        console.log("ERROR", error.sqlMessage)
      }
    });
    
  }).catch((error)=> {
    ctx.reply('Server error, silahkan coba lagi nanti')
  })
})

//add truck

bot.command('addtruck', (ctx)=> { 
  db.query({
    sql: 'SELECT * FROM `truck_models`',
    timeout: 40000, // 40s
  }, function(error, results, fields) {
      models = results;
      ctx.scene.enter('add-truck')
    if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });
});
bot.command('addmodeltruck', (ctx)=>  ctx.scene.enter('add-truckmodel'));
bot.command('addorder', (ctx)=>  {
  db.query({
    sql: 'SELECT * FROM `users`',
    timeout: 40000, // 40s
  }, function(error, results, fields) {
      receivers = results;
      db.query({
        sql: 'SELECT * FROM `users` WHERE chat_id = ?',
        timeout: 40000,
        values: [ctx.chat.id]
      }, function(error, results, fields) {
          orderObject.sender_id = results[0].id
          orderObject.sender_name = results[0].name
          orderObject.sender_address = results[0].address
          orderObject.sender_chat_id = ctx.chat.id;
          ctx.scene.enter('add-order')
          if (error){
          console.log("ERROR", error.sqlMessage)
        }
      });
      if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });

});
bot.command('addroute', (ctx)=>  {
  db.query({
    sql: 'select t.id, t.model_id, t.code, t.vendor, t.series, mt.name, mt.capacity from trucks t inner join truck_models mt on t.model_id=mt.id;',
    timeout: 40000, // 40s
  }, function(error, results, fields) {
      trucksdata = results;
      db.query({
        sql: 'SELECT * FROM `drivers`',
        timeout: 40000,
      }, function(error, results, fields) {
          driversdata = results;
          db.query({
            sql: 'select o.id, s.name sender_name,r.name receiver_name, ori.id origin_id, ori.address origin_address, dest.id destination_id, dest.address destination_address from orders o inner join users s on o.sender_id=s.id inner join users as r on o.receiver_id=r.id inner join points ori on o.origin_id=ori.id inner join points dest on o.destination_id=dest.id;',
            timeout: 40000,
          }, function(error, results, fields) {
              ordersdata = results;
              ctx.scene.enter('add-route')
              if (error){
              console.log("ERROR", error.sqlMessage)
            }
          });
          if (error){
          console.log("ERROR", error.sqlMessage)
        }
      });
      if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });


});
bot.command('confirmroute', (ctx)=>  ctx.scene.enter('confirm-route'));
bot.command('issuewaybill', (ctx)=>  {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray[0];
  db.query({
    sql: 'select rp.order_id,r.truck_id, t.code truck_code,r.driver_id, d.name driver_name, d.chat_id driver_chat_id, o.sender_id,os.name sender_name,osp.address origin_address, osp.latitude origin_latitude, osp.longitude origin_longitude, os.chat_id sender_chat_id, o.receiver_id, orec.name receiver_name, orp.address destination_address, orp.latitude destination_latitude, orp.longitude destination_longitude, orec.chat_id receiver_chat_id from route_paths rp inner join routes r on rp.route_id=r.id inner join trucks t on r.truck_id = t.id inner join drivers d on r.driver_id=d.id inner join orders o on rp.order_id = o.id inner join users os on o.sender_id=os.id inner join users orec on o.receiver_id=orec.id inner join points osp on o.origin_id=osp.id inner join points orp on o.destination_id=orp.id where rp.order_id= ?;',
    timeout: 40000, // 40s
    values:[id]
  }, function(error, results, fields) {
    if (results.length > 0) {
      waybilldata = results[0];
      db.query({
        sql: 'select count(*) as c from order_items oi where oi.order_id= ?;',
        timeout: 40000, // 40s
        values:[id]
      }, function(error, results, fields) {
          let numItem = results[0].c;
          waybilldata.numItem = numItem;
        ctx.scene.enter('issue-waybill')
     });
    } else {
      ctx.reply('ID Order tidak ditemukan')
    }
    
      if (error){
        console.log("ERROR", error.sqlMessage)
        ctx.reply('ID order tidak ditemukan')
      }
      
  });

});
bot.command('confirmsenderwaybill', (ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray[0];
  waybillObject.confirm_sender = true;
  ctx.telegram.sendMessage(pt_id, `Konfirmasi dari produsen diterima untuk waybill ${id}`);
  if (waybillObject.confirm_driver && waybillObject.confirm_receiver) {
    ctx.scene.enter('waybill-confirmed')
  }
})

bot.command('confirmreceiverwaybill', (ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray[0];
  waybillObject.confirm_receiver = true;
  ctx.telegram.sendMessage(pt_id, `Konfirmasi dari konsumen diterima untuk waybill ${id}`);
  if (waybillObject.confirm_sender && waybillObject.confirm_driver) {
    ctx.scene.enter('waybill-confirmed')
  }
})

bot.command('confirmdriverwaybill', (ctx)=>  ctx.scene.enter('confirm-driver-waybill'));

bot.command('securitycheck', (ctx)=>  ctx.scene.enter('security-check'))

bot.command('confirmload', (ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray.join();
  db.query({
    sql: 'SELECT d.chat_id FROM waybills w inner join drivers d on w.driver_id = d.id  WHERE w.id = ?',
    timeout: 40000, // 40s
    values: [id]
  }, function(error, results, fields) {
      let driver_id = results[0].chat_id;
      ctx.telegram.sendMessage(driver_id, `Konfirmasi pemuatan untuk waybill ${id}, silahkan login di web kemudian konfirmasi dengan menggunakan perintah /confirmloaddriver SPASI {ID Waybill}`)
      ctx.reply(`Konfirmasi pemuatan waybill ${id} dari produsen sukses, menunggu konfirmasi dari sopir`);
      if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });
})

bot.command('confirmunload', (ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray.join();
  db.query({
    sql: 'SELECT d.chat_id FROM waybills w inner join drivers d on w.driver_id = d.id  WHERE w.id = ?',
    timeout: 40000, // 40s
    values: [id]
  }, function(error, results, fields) {
      let driver_id = results[0].chat_id;
      ctx.telegram.sendMessage(driver_id, `Konfirmasi bongkar muatan untuk waybill ${id}, silahkan login di web kemudian konfirmasi dengan menggunakan perintah /confirmunloaddriver SPASI {ID Waybill}`)
      ctx.reply(`Konfirmasi bongkar muatan waybill ${id} dari konsumen sukses, menunggu konfirmasi dari sopir`);
      if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });
})

bot.command('confirmloaddriver', (ctx)=>  ctx.scene.enter('confirm-load-driver'));

bot.command('confirmunloaddriver', (ctx)=>  ctx.scene.enter('confirm-unload-driver'));

bot.command('tracking', (ctx)=>  {
  
  if (ctx.message.chat.id == pt_id) {
  ctx.reply('Pilih Jenis Riwayat', Markup.inlineKeyboard([
    Markup.callbackButton('Truk', 'truck-history'),
    Markup.callbackButton('Pesanan', 'order-history'),
    Markup.callbackButton('Rute', 'route-history'),
    Markup.callbackButton('Waybill', 'waybill-history'),
  ]).extra())
} else {
  db.query({
    sql: 'SELECT * FROM users where chat_id = ?',
    timeout: 40000, // 40s
    values: [ctx.message.chat.id]
  }, function(error, results, fields) {
      if(results.length > 0){
        ctx.reply('Pilih Jenis Riwayat', Markup.inlineKeyboard([
          Markup.callbackButton('Pesanan', 'order-history'),
          Markup.callbackButton('Waybill', 'waybill-history'),
        ]).extra())
      } else {
        ctx.reply('Anda belum teregistrasi di sistem')
      }
      if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });
  
} 
});

bot.command('trackingdriver', (ctx)=>{
  db.query({
    sql: 'SELECT * FROM drivers where chat_id = ?',
    timeout: 40000, // 40s
    values: [ctx.message.chat.id]
  }, function(error, results, fields) {
      if(results.length > 0){
        ctx.reply('Pilih Jenis Riwayat', Markup.inlineKeyboard([
          Markup.callbackButton('Rute', 'route-history'),
          Markup.callbackButton('Waybill', 'waybill-history'),
        ]).extra())
      } else {
        ctx.reply('Anda belum teregistrasi di sistem')
      }
      if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });
})

bot.command('confirmpayment', (ctx)=>  ctx.scene.enter('confirm-payment'))

bot.command('receivepayment', (ctx) => {
  let messageArray = ctx.message.text.split(' ');
  messageArray.shift();
  const id = messageArray.join();
  ctx.reply(`Pembayaran pesanan ${id} telah dikonfirmasi`);
  db.query({
    sql: 'SELECT chat_id FROM users u inner join orders o on u.id=o.sender_id WHERE o.id = ?',
    timeout: 40000, // 40s
    values: [id]
  }, function(error, results, fields) {
      let produsen_id = results[0].chat_id;
      let paymentOrderQuery = {
        type: 'update',
        table: 'orders',
        modifier: {
          payment_status: 'PAID'
        },
        condition: {
          id: id
        }
      }
      axios.request({
        baseURL:`http://${peers[peersindex]}`,
        url: '/createBlock/database',
        method: 'post',
      data: {block: {data:paymentOrderQuery}}
      }).then((res)=>{
        dateInput = getCurrentTimestamp();
        axios.request({
          baseURL:`http://${peers[peersindex]}`,
          url: `/createBlock/${id}`,
          method: 'post',
          data: {block: {data:{status: 'COMPLETED', payment_status: 'PAID', date:dateInput}}}
        }).then(()=>{
          ctx.telegram.sendMessage(produsen_id, `Pembayaran pesanan ${id} telah dikonfirmasi`);
        }).catch((error)=> {
          ctx.reply('Server error, silahkan coba lagi nanti')
        })
      }).catch((error)=> {
        ctx.reply('Server error, silahkan coba lagi nanti')
      })
    if (error){
      console.log("ERROR", error.sqlMessage)
    }
  });
})

bot.on('location', (ctx) => {
  console.log(ctx.message.location);
})


// https://github.com/cozmo/jsQR/issues/16#issuecomment-526009537
// modified to use jpeg
function parseQRCode(url) {
  return axios.get(url, {responseType: 'arraybuffer'}).then((res)=>{
    let img = jpeg.decode(res.data)
    let data = new Uint8Array(img.data)
    return jsQR(data, img.width, img.height).data
  })
}

bot.catch((err, ctx) => {
  console.log(err);
  console.log(`Encountered an error for ${ctx.updateType}`, err)
  ctx.reply('Error, pastikan input valid');
  ctx.scene.leave();
})

bot.launch()

axios.interceptors.response.use(null, (error) => {
  if (error.config) {
    roundRobinPeers()
    count++;
    console.log(count);
    console.log(peers[peersindex])
    console.log('round baru')
    let newconfig = error.config;
    newconfig.baseURL = `http://${peers[peersindex]}`
    if (!(count >= peers.length)) {
      return axios.request(newconfig)
    } else {
      count = 0;
      throw Error('all peers disconnected');
      // return Promise.reject(error).then(()=>{},(error)=>{console.log('all peers disconnected'); throw Error;});
    }
  }

  
  
});


module.exports = app;
